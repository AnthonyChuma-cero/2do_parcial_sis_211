/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.do_parcial_sis_211;

//import static java.lang.reflect.Array.get;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
//import com.mycompany.Duenio;
//import com.mycompany.Automovil;

/**
 *
 * @author PERSONAL
 */
public class Main{
    private static final ArrayList<Automovil> automoviles = new ArrayList<>();
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.println("-----------MENU------------");
            System.out.println("1. Registrar el automovil");
            System.out.println("2. Listar todos los automovil");
            System.out.println("3. Buscar un automovil");
            System.out.println("4. Eliminar un automovil");
            System.out.println("5. Ver Informacion del automovil");
            System.out.println("6. Ver Informacion del duenio");
            System.out.println("7. Eliminar registro");
            System.out.println("8. >>>>>>>>Salir");
            System.out.println();
            System.out.print("Ingrese una opcion: ");
            int opcion = sc.nextInt();
            sc.nextLine();
            
            try{
                switch(opcion){
                    case 1:
                        try{
                            RegistrarAutomovil(sc);
                        }catch(InputMismatchException e) {
                            System.out.println("Error de entrada invalida: " + e.getMessage());
                            sc.nextLine();
                        }
                        break;
                    case 2:
                        ListarAutomoviles();
                        break;
                    case 3:
                        try{
                            BuscarAutomovil(sc);
                        }catch(InputMismatchException e) {
                            System.out.println("Error de entrada invalida: " + e.getMessage());
                            sc.nextLine();
                        }
                        break;
                    case 4:
                        try{
                            EliminarAutomovil(sc);
                        }catch(InputMismatchException e) {
                            System.out.println("Error de entrada invalida: " + e.getMessage());
                            sc.nextLine();
                        }
                        break;
                    case 5:
                        try{
                            VerInformacionAutomovil(sc);
                        }catch(InputMismatchException e) {
                            System.out.println("Error de entrada invalida: " + e.getMessage());
                            sc.nextLine();
                        }
                        break;
                    case 6:
                        try{
                            VerInformacionDuenio(sc);
                        }catch(InputMismatchException e) {
                            System.out.println("Error de entrada invalida: " + e.getMessage());
                            sc.nextLine();
                        }
                        break;
                    case 7:
                        try{
                           EliminarRegistro();
                        }catch(InputMismatchException e) {
                            System.out.println("Error de entrada invalida: " + e.getMessage());
                            sc.nextLine();
                        }
                        break;
                    case 8:
                        System.out.println("Saliendo del programa...");
                        System.exit(0);
                    default:
                        System.out.println("Opcion Invalida. Por favor, Intente Nuevamente!");
                }
            }catch(Exception e){
                System.out.println("Error: " + e.getMessage());
            }
        }     
    }
    private static void RegistrarAutomovil(Scanner scan){
        try{
            System.out.print("Igrese La Placa Del Automovil: ");
            String placa = scan.nextLine();
            System.out.print("Ingrese El Color Del Automovil: ");
            String color = scan.nextLine();
            System.out.print("Ingrese El Modelo Del Automovil: ");
            String modelo = scan.nextLine();
            System.out.print("Ingrese El Anio De Modelo Del Automovil: ");
            int anio = scan.nextInt();
            scan.nextLine(); 
            System.out.print("Ingrese El Número De Puertas Del Automovil: ");
            int numeroDePuertas = scan.nextInt();
            scan.nextLine();
            System.out.print("Ingrese La Fecha De Compra Del Automovil: ");
            String fechaDeCompra = scan.nextLine();
            System.out.print("Ingrese El Nombre Completo Del Duenio: ");
            String nombreCompleto = scan.nextLine();
            System.out.print("Ingrese El Apellido Del Duenio: ");
            String apellido = scan.nextLine();
            System.out.print("Ingrese El Departamento de Nacimiento Del Duenio: ");
            String departamentoDeNacimiento = scan.nextLine();
            System.out.print("Ingrese El Anio De Nacimiento Del Duenio: ");
            String anioDeNacimiento = scan.nextLine();
            scan.nextLine();
        
            Duenio duenio = new Duenio(nombreCompleto, apellido, departamentoDeNacimiento, anioDeNacimiento);
            Automovil automovil = new Automovil(placa, color, modelo, anio, numeroDePuertas, fechaDeCompra, duenio);
            automoviles.add(automovil);
        
            System.out.println("Automovil Registrado Exitosamente.");
            
        }catch (Exception e){
            System.out.println("Error al registrar el automovil: " + e.getMessage());
        }
    }
    private static void ListarAutomoviles() {
        try{
            if (automoviles.isEmpty()){
                System.out.println("No Hay Automoviles Registrados.");
            }else {
                System.out.println("AUTOMOVILES REGISTRADOS:");
                for (Automovil automovil : automoviles) {
                    System.out.println("Placa: " + automovil.getPlaca() + ", Modelo: " + automovil.getModelo() + ", Color: " + automovil.getColor());
                }
            }
            
        }catch (Exception e){
            System.out.println("Error al listar los automoviles: " + e.getMessage());
        }
    }
    private static void BuscarAutomovil(Scanner sc) {
        try{
            System.out.print("Ingrese La Placa Del Automovil A Buscar: ");
            String placa = sc.nextLine();
            boolean encontrado = false;
            for (Automovil automovil : automoviles) {
                if (automovil.getPlaca().equals(placa)) {
                    System.out.println("AUTOMOVIL ENCONTRADO: ");
                    System.out.println("Placa: " + automovil.getPlaca());
                    System.out.println("Modelo: " + automovil.getModelo());
                    System.out.println("Color: " + automovil.getColor());
                    System.out.println("Duenio: " + automovil.getDuenio().getNombreCompleto() + " " + automovil.getDuenio().getApellido());
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado){
                System.out.println("Automovil No Encontrado.");
            }
        }catch (Exception e){
            System.out.println("Error al buscar el automovil: " + e.getMessage());
        }
    }
    private static void EliminarAutomovil(Scanner sc) {
        try{
            System.out.print("Ingrese La Placa Del Automovil A Eliminar: ");
            String placa = sc.nextLine();

            boolean encontrado = false;
            for (Automovil automovil : automoviles) {
                if (automovil.getPlaca().equals(placa)) {
                    automoviles.remove(automovil);
                    encontrado = true;
                    System.out.println("Automovil Eliminado Exitosamente.");
                    break;
                }
            }
            if (!encontrado){
                System.out.println("Automovil No Encontrado.");
            }
        }catch (Exception e){
            System.out.println("Error al eliminar el automovil: " + e.getMessage());
        }       
    }
    private static void VerInformacionAutomovil(Scanner sc) {
        try{
            System.out.print("Ingrese La Placa Del Automovil: ");
            String placa = sc.nextLine();
            boolean encontrado = false;
            for (Automovil automovil : automoviles) {
                if (automovil.getPlaca().equals(placa)) {
                    System.out.println("INFORMACION DEL AUTOMOVIL:");
                    System.out.println("Placa: " + automovil.getPlaca());
                    System.out.println("Modelo: " + automovil.getModelo());
                    System.out.println("Color: " + automovil.getColor());
                    System.out.println("Anio de Modelo: " + automovil.getAnio());
                    System.out.println("Numero de Puertas: " + automovil.getNumeroDePuertas());
                    System.out.println("Fecha de Compra: " + automovil.getFechaDeCompra());
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado){
                System.out.println("Automovil No Encontrado.");
            }
        }catch (Exception e){
            System.out.println("Error al ver la informacion del automovil: " + e.getMessage());
        }
    }
    private static void VerInformacionDuenio(Scanner sc) {
        try{
            System.out.print("Ingrese La Placa Del Automovil: ");
            String placa = sc.nextLine();
            boolean encontrado = false;
            for (Automovil automovil : automoviles) {
                if (automovil.getPlaca().equals(placa)) {
                    System.out.println("INFORMACION DEL DUENIO:");
                    System.out.println("Nombre Completo: " + automovil.getDuenio().getNombreCompleto());
                    System.out.println("Apellido: " + automovil.getDuenio().getApellido());
                    System.out.println("Departamento de Nacimiento: " + automovil.getDuenio().getDepartamentoDeNacimiento());
                    System.out.println("Anio de Nacimiento: " + automovil.getDuenio().getAnioDeNacimiento());
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado){
                System.out.println("Automovil No Encontrado.");
            }
        }catch (Exception e){
            System.out.println("Error al ver la información del duenio: " + e.getMessage());
        }
    }
    private static void EliminarRegistro() {
        try{
            automoviles.clear();
            System.out.println("Registros Eliminados Exitosamente.");
        }catch (Exception e){
            System.out.println("Error al eliminar el registro: " + e.getMessage());
        }
    }
}