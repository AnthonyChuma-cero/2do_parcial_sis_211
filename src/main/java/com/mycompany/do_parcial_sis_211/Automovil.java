/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.do_parcial_sis_211;

/**
 *
 * @author PERSONAL
 */
class Automovil {
    private String placa;
    private String color;
    private String modelo;
    private int anio;
    private int numeroDePuertas;
    private String fechaDeCompra;
    private Duenio duenio;
    
    public Automovil(String placa,String color, String modelo,int anio,int numeroDePuertas,String fechaDeCompra,Duenio duenio){
        this.placa = placa;
        this.color = color;
        this.modelo = modelo;
        this.anio = anio;
        this.numeroDePuertas = numeroDePuertas;
        this.fechaDeCompra = fechaDeCompra;
        this.duenio = duenio;
    }
    public String getPlaca(){
        return placa;
    }
    public void setPlaca(String placa){
        this.placa = placa;
    }
    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color = color;
    }
    public String getModelo(){
        return modelo;
    }
    public void setModelo(String modelo){
        this.modelo = modelo;
    }
    public int getAnio(){
        return anio;
    }
    public void setAnio(int anio){
        this.anio = anio;
    }
    public int getNumeroDePuertas() {
        return numeroDePuertas;
    }
    public void setNumeroDePuertas(int numeroDePuertas) {
        this.numeroDePuertas = numeroDePuertas;
    }
    public String getFechaDeCompra() {
        return fechaDeCompra;
    }
    public void setFechaDeCompra(String fechaDeCompra) {
        this.fechaDeCompra = fechaDeCompra;
    }    
    public Duenio getDuenio(){
        return duenio;
    }    
    public void setDuenio(Duenio duenio){
        this.duenio = duenio;
    }
    
     
    public String VerInformacionAutomovil(){
        return this.placa +" "+ this.modelo +" "+ this.color;
    }
    public String VerInformacionDuenio(){
        return this.placa +" "+ this.modelo +" "+ this.duenio.getNombreCompleto();
    }
    public String EliminarRegistro(){
        placa = null;
        color = null;
        modelo = null;
        anio = 0;
        numeroDePuertas = 0;
        fechaDeCompra = null;
        duenio = null;    
        return null;
    }
}