/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.do_parcial_sis_211;

/**
 *
 * @author PERSONAL
 */
class Duenio {
    private String nombreCompleto;
    private String apellido;
    private String departamentoDeNacimiento;
    private String anioDeNacimiento;
    
    public Duenio(String nombreCompleto,String apellido, String departamentoDeNacimiento,String anioDeNacimiento){
        this.nombreCompleto = nombreCompleto;
        this.apellido = apellido;
        this.departamentoDeNacimiento = departamentoDeNacimiento;
        this.anioDeNacimiento = anioDeNacimiento;
    }
    public String getNombreCompleto(){
        return nombreCompleto;
    }
    public void setNombreCompleto(String nombreCompleto){
        this.nombreCompleto = nombreCompleto;
    }
    public String getApellido(){
        return apellido;
    }
    public void setColor(String apellido){
        this.apellido = apellido;
    }
    public String getDepartamentoDeNacimiento(){
        return departamentoDeNacimiento;
    }
    public void setDepartamentoDeNacimiento(String departamentoDeNacimiento){
        this.departamentoDeNacimiento = departamentoDeNacimiento;
    }
    public String getAnioDeNacimiento(){
        return anioDeNacimiento;
    }
    public void setAnioDeNacimiento(String anioDeNacimiento){
        this.anioDeNacimiento = anioDeNacimiento;
    }
}
